
# Changelog for Messages Portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v6.5.0-SNAPSHOT] - 2022-10-20

- Updatedtge D4Science Logo



## [v1.0.0] - 2013-01-13

- First release
